from django.urls import path
from cart.views import index, charge, checkout, customer

urlpatterns = [
    path("", index, name="index"),
    path("charge/", charge, name="charge"),
    path("checkout/<int:cookies>", checkout, name="checkout"),
    path("customer/", customer, name="customer"),
]
