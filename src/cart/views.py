from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
import stripe
import pymongo

from cart.forms import CreateUserForm, CreditCardForm
from cart.models import UserProfile
from main import settings

client = pymongo.MongoClient("localhost")
db = client["shopping_cart"]

stripe.api_key = "sk_test_4eC39HqLyjWDarjtT1zdp7dc"

def index(request):
    return render(request, "index.html")

def charge(request, cookies):
    return render(request, "charge.html")

def customer(request):
    return None

@login_required(login_url="login")
def checkout(request, cookies):
    form = None
    if request.method == "POST":
        form = CreditCardForm(request.POST)
        if form.is_valid():
            number = form.cleaned_data.get("credit_card_number")
            exp_month = form.cleaned_data.get("month")
            exp_year = form.cleaned_data.get("year")
            cvc = form.cleaned_data.get("cvc")

            email = request.user.email

            try:
                card_token = stripe.Token.create(
                    card={
                        "number": number,
                        "exp_month": int(exp_month),
                        "exp_year": int(exp_year),
                        "cvc": cvc,
                    },
                )

                print(card_token)

                customer = db.customer.find_one({"email":{"$eq":email}})
                print(customer)
                print(customer["id"])


                charge = stripe.Charge.create(
                    amount=cookies * 5 * 100,
                    currency="usd",
                    description="buying cookies test",
                    source=card_token,
                    idempotency_key=card_token.id
                    #idempotency_key=f"cookies_{str(cookies*5)}{email}",
                    #customer=customer["id"]
                )

                print(charge)
                db.charge.insert_one(charge)

                result = db.card.insert_one({
                    #"customer" customer["id"]
                    "number": f"xxxx-xxxx-xxxx-{number[-4:]}",
                    "month": exp_month,
                    "year": exp_year,
                    "cvc": "x"*len(str(cvc))
                })

                print(result)

                if charge["status"] == "succeeded":
                    messages.success(request, f"succeeded")
                    messages.info(request, f"Thanks you paid ${cookies*5}")
                    return render(request, "charge.html")
            except stripe.error.CardError as e:
                messages.info(request, e.error.message)

            except stripe.error.RateLimitError as e:
                # Too many requests made to the API too quickly
                messages.info(request, e.error.message)

            except stripe.error.InvalidRequestError as e:
                # Invalid parameters were supplied to Stripe's API
                messages.info(request, e.error.message)

            except stripe.error.AuthenticationError as e:
                # Authentication with Stripe's API failed
                # (maybe you changed API keys recently)
                messages.info(request, e.error.message)

            except stripe.error.APIConnectionError as e:
                # Network communication with Stripe failed
                messages.info(request, e.error.message)

            except stripe.error.StripeError as e:
                # Display a very generic error to the user, and maybe send
                # yourself an email
                messages.info(request, e.error.message)

            except Exception as e:
                # Something else happened, completely unrelated to Stripe
                messages.info(request, e.error.message)

    context = {
        "price": 5,
        "amount": cookies,
        "total": cookies * 5,
        "cookies": [i for i in range(cookies)],
        "form": CreditCardForm() if not form else form
    }

    return render(request, "checkout.html", context)

def register_page(request):
    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            email = form.cleaned_data.get("email")

            customer = stripe.Customer.create(
                description="Cookie seller test",
                email=email,
                name=username,
            )

            profile = UserProfile.objects.create()
            profile.user = User.objects.filter(email=email).first()
            profile.customer_id = customer.id

            profile.save()
            db.customer.insert_one(customer)

            messages.success(request, f"Account created for {username} -- customer id {customer.id}")
            return redirect("login")


    context = {"form":form}
    return render(request, "user/register.html", context)
