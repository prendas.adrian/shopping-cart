from django.contrib import admin

from cart.models import Article

admin.site.register(Article)
