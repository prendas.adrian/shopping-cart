from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    customer_id = models.CharField(max_length=250)


class Article(models.Model):
    description = models.CharField(max_length=250)
    price = models.FloatField()
    amount = models.IntegerField()
